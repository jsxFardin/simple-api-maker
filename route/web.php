<?php
require_once "env.php";
require_once "function.php";


$page     = isset($_GET['page']) ? $_GET['page'] : null;
$method   = isset($_GET['method']) ? $_GET['method'] : null;
$route_file     = "app/{$page}.php";


if (file_exists($route_file)) {
    include_once "app/{$page}.php";
    $obj = new $page();
    if (method_exists($obj, $method)) {
        $obj->$method();
    } else {
        $api_header = [
            "status"        => 404,
            "success"       => false,
            "message"       => "Method not found!",
            "description"   => "Please request to correct method with valid data!",
        ];
        output($api_header);
    }
} else {
    $api_header = [
        "status"        => 404,
        "success"       => false,
        "message"       => "App not found!",
        "description"   => "Please request to correct app with valid data!",
    ];
    output($api_header);
}
