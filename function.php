<?php
/* 
    * tow array required
    * $header (status : requird , success  )
*/

function output($header, $data = null)
{
    $output = [
        'status' => $header['status'],
        'success' => $header['success'],
        'message' => $header['message'],
        'description' => !$header['description'] ? null : $header['description'],
        'data' => !$data ? [] : $data
    ];

    if (empty($output['data'])) {
        unset($output['data']);
    }
    if ($output['description'] == null) {
        unset($output['description']);
    }
    die(json_encode($output));
}

function _throw_exception($msg_title, $msg_desc = null)
{
    throw new Exception("{$msg_title}|{$msg_desc}");
}

function d($data = null)
{
    echo '<pre>';
    var_dump($data);
    echo '</pre>';
}
function p($data = null)
{
    echo '<pre>';
    print_r($data);
    echo '</pre>';
}
function dd($data = null)
{
    d($data);
    die('-- died --');
}
function pd($data = null)
{
    p($data);
    die('-- died --');
}
